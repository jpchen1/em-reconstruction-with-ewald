# Electron microscopy reconstruction with Ewald curvature

The codes in this repository reconstructs a 3D density given a series of 2D projection images affected by the curvature of the Ewald sphere.

### Files:
- main.py - The main script.
- input.py - Contains functions to simulate an input object potential.
- core_functions.py - Functions for reconstructing from curved Ewald spheres.
- show_results_single.py - Plots results from a single reconstruction run (saves the figures as .png files in the present working directory).
- show_results_pair.py - Reproduces the figures from [1] (saves the figures as .png files in the present working directory).

### To reproduce the figures in the paper:
Run `main.py` twice, once with `is_proper_algo` set to `True` and another time with `is_proper_algo` set to `False`. This will generate two .npz files containing the results of the two reconstructions, one with the proper algorithm and one with the algorithm assuming flat Ewald sphere. Then running `show_results_pair.py` will generate the same types of figures seen in [1].

### References:
[1] J. P. J. Chen, K. E. Schmidt, J. C. H. Spence and R. A. Kirian. “A new solution to the curved Ewald sphere problem for 3D image reconstruction in electron microscopy” _Ultramicroscopy_, **224**, 113234 (2021).
