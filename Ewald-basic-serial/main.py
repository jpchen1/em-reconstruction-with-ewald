"""
Script to reconstruct a 3D object from 2D "views" affected by Ewald curvature.
See the paper below for more details on the algorithm:
https://arxiv.org/abs/2101.11709

You need the reborn package. See: 
https://kirianlab.gitlab.io/reborn/index.html

Notes:
- Proper Euler angle convention: zyz

Date Created: 24 Oct 2020
Last Modified: 19 Aug 2021
Author: JC
"""

import numpy as np
import sys

from input import get_data
from core_functions import *

np.random.seed(42)

#==============================================================================
# Program parameters
is_proper_algo = True # Select whether or not to incorporate Ewald curvature in the algorithm
res = 'low'
ew = 0.5 # Ewald number
run_num = 1


# Starting iterate select
# 0 - true solution
# 1 - random
# 2 - perturbed solution
starting_iterate = 1

# Rotation scenario
# Scenario 0 - No rotation
# Scenario 1 - Specified rotation - Not implemented yet
# Scenario 2 - Regularly sampled rotation in z
# Scenario 3 - Regularly sampled rotation in y
# Scenario 4 - Regularly sampled rotation in all three directions (TODO)
# Scenario 5 - Random rotation
rotation_scenario = 5

# Defocus scenario
# Scenario 0 - Tomographic limit
# Scenario 1 - All focused on the central plane 
# Scenario 2 - Random focus ranging from |...|obj|...|
# Scenario 3 - Through-focus series
defocus_scenario = 2


pdb_file_path = '../data/1TNV.pdb'


# Resolution
if res == 'low':
	N_worker = 3
	it_outer_max = 1
	it_inner_max = 5
	N_views_factor = 0.5

	resolution = 12e-10 # in metres

	lamb = (2 * (resolution*1e9)**2)/(ew * 29)

	# Amount of padding to add (needs to be even) for rotation and propagation purposes. 
	# The size of the overall array is approx N0*np.sqrt(2)+N_padding*2 
	N_padding = 0
	
elif res == 'med':
	N_worker = 3
	it_outer_max = 3
	it_inner_max = 40
	N_views_factor = 1.0

	resolution = 5e-10 # in metres

	lamb = (2 * (resolution*1e9)**2)/(ew * 29)

	# Amount of padding to add (needs to be even) for rotation and propagation purposes. 
	# The size of the overall array is approx N0*np.sqrt(2)+N_padding*2 
	N_padding = 20
	
elif res == 'high':
	N_worker = 5
	it_outer_max = 3
	it_inner_max = 10
	N_views_factor = 0.5

	resolution = 1.6e-10 # in metres
	lamb = 0.0037064885944591355 # (nm) #100keV => 0.0037064885944591355 nm #400keV => 0.0016466315086405426 nm 
	# Amount of padding to add (needs to be even) for rotation and propagation purposes. 
	# The size of the overall array is approx N0*np.sqrt(2)+N_padding*2 
	N_padding = -28



it_inner_cycle = 300
it_inner_IPA = 200

# IPA select
IPA_sel = 'RAAR'

# IPA parameters
if (IPA_sel == 'DM'):
	beta = 0.7
	gamma_M = -1/beta
	gamma_S = 1/beta

elif (IPA_sel == 'RAAR'):
	beta = 0.7


if is_proper_algo == True:
	save_filename = 'run%s_Nworker=%d_out=%d_in=%d' % (run_num, N_worker, it_outer_max, it_inner_max)
else:
	save_filename = 'run%s_flatEwaldAlgo_Nworker=%d_out=%d_in=%d' % (run_num, N_worker, it_outer_max, it_inner_max)


#========================================================================
# Input set-up
print("Input setup")

I_data_list, f_true_os, container = get_data(pdb_file_path, resolution, lamb, N_padding, N_views_factor, rotation_scenario, defocus_scenario, is_proper_algo)

N_slices = container['N_slices']
theta_list = container['theta_list']
z_mn_list = container['z_mn_list']
Nz_sta = container['Nz_sta']
Nz_end = container['Nz_end']
Nadd = container['Nadd']
q_z_mesh = container['q_z_mesh']
N_views = container['N_views']
Nx_os = container['Nx_os']
Ny_os = container['Ny_os']
Nz_os = container['Nz_os']
Delta_z = container['Delta_z']
Nx_sta = container['Nx_sta']
Ny_sta = container['Ny_sta']
Nz_sta = container['Nz_sta']
Nx_end = container['Nx_end']
Ny_end = container['Ny_end']
Nz_end = container['Nz_end']
N = container['N']


#========================================================================
# Define the projectors

def P_S(x, S_in):
	"""
	The concur projection.
	"""

	# Unload content in S_in
	N_p_curr = S_in['N_p_curr']
	InDict_x2f = S_in['InDict_x2f']
	InDict_f2x = S_in['InDict_f2x']

	# Inverse propagate and average
	f_new = x2f(x, N_p=N_p_curr, InDict=InDict_x2f)

	# Apply reality and positivity constraints
	f_new = np.real(f_new)
	f_new[f_new < 0] = 0

	# Forward propagate and distribute
	x_new = f2x(f_new, N_p=N_p_curr, InDict=InDict_f2x)

	return x_new



def P_M(x, M_in):
	"""
	The data projection.
	"""

	# Unload content in M_in
	I_data_list = M_in['Fourier Intensity list']
	InDict_calc_Iandc = M_in['InDict_calc_Iandc']
	N_p_curr = M_in['N_p_curr']
	N_s = M_in['N_s']

	# Form the current I
	I_curr_list, _ = calc_Iandc(x, N_p=N_p_curr, InDict=InDict_calc_Iandc)

	# Do the projection.
	# Leave the real part unchanged and subtract the difference between the 
	# current sum and the data from the imaginary part.
	x_new = [[0 for n_s in range(N_s)] for n_p in range(N_p_curr)]
	for n_p in range(N_p_curr):
		difference_curr = (I_curr_list[n_p] - I_data_list[n_p]) / N_s
		for n_s in range(N_s):
			x_new[n_p][n_s] = np.real(x[n_p][n_s]) + 1j*(np.imag(x[n_p][n_s]) - difference_curr)

	return x_new


#========================================================================
# Define IPA

def RAAR(x, beta, InDict_IPA, M_in, S_in):
	"""
	A single Relaxed-Averaged-Alternating Reflections step.
	"""
	N_p_curr = InDict_IPA['N_p_curr']
	N_s = InDict_IPA['N_s']

	x_PM = P_M(x, M_in)
	x_PS = P_S(x, S_in)
	x_PSPM = P_S(x_PM, S_in)

	# Create storage for the modified iterate
	x_new = [[0 for n_s in range(N_s)] for n_p in range(N_p_curr)]
	for n_p in range(N_p_curr):
		for n_s in range(N_s):
			x_new[n_p][n_s] = 2*beta * x_PSPM[n_p][n_s]     \
			                  - beta * x_PS[n_p][n_s]       \
			                  + (1-2*beta) * x_PM[n_p][n_s] \
			                  + beta * x[n_p][n_s]

	return x_new, x_PS, x_PM

#========================================================================
# Reconstruction start

InDict_calc_Iandc = {'Nx_os':Nx_os,
				     'Ny_os':Ny_os,
				     'N_slices':N_slices}


# Figure out how much work each workers get
N_work_total = N_views

# First workers get this much:
N_work_normal = int(np.ceil(N_work_total / N_worker))

# Last worker gets this much:
N_work_last = N_work_total - (N_worker-1) * N_work_normal


work_ind_sta_vec = np.arange(0, N_work_total, N_work_normal) 
work_ind_end_vec = np.append(work_ind_sta_vec[1:], N_work_total)


print('N_work_total:', N_work_total)
print('N_worker:', N_worker)
print('N_work_normal:', N_work_normal)
print('N_work_last:', N_work_last)
print('work_ind_sta_vec:', work_ind_sta_vec)
print('work_ind_end_vec:', work_ind_end_vec)


#--------------------
# Initialisations 

# Total number of iterations
it_total = it_outer_max * it_inner_max

# Error storage
errors = np.zeros([it_total,2])
errors_PM = np.zeros(it_total)

# Iterate to be passed to the next outer iteration
f_avg = 0
#--------------------

# Outer loop
for it_outer in range(it_outer_max):
	print("it_outer:", it_outer)

	# Reset the variables that are used to hold the sums
	f_sum_curr = 0
	errors_inner_0_sum = 0
	errors_inner_1_sum = 0

	# Best iterate storage
	x_best = 0
	E_inner_best = np.inf
	errors_inner = np.zeros([it_inner_max,2])
	
	# Worker loop	
	for n_worker in range(N_worker):
		sys.stdout.write("n_worker: %d \n" % n_worker)
		sys.stdout.write("[%d:%d] \n" % (work_ind_sta_vec[n_worker], work_ind_end_vec[n_worker]))
		sys.stdout.flush()

		I_data_list_curr = I_data_list[work_ind_sta_vec[n_worker] : work_ind_end_vec[n_worker]]
		N_p_curr = len(I_data_list_curr)

		theta_list_curr = theta_list[:, work_ind_sta_vec[n_worker]:work_ind_end_vec[n_worker]]
		z_mn_list_curr = z_mn_list[work_ind_sta_vec[n_worker]:work_ind_end_vec[n_worker], :]


		# Update the input dictionaries - i.e. things needed for the f2x and x2f functions
		InDict_f2x = {'N_slices':N_slices,
					  'theta_list':theta_list_curr,
					  'z_mn_list':z_mn_list_curr,
					  'Nz_sta':Nz_sta,
					  'Nz_end':Nz_end,
					  'Nadd':Nadd,
					  'q_z_mesh':q_z_mesh,
					  'Delta_z':Delta_z}

		InDict_x2f = {'Nx_os':Nx_os,
					  'Ny_os':Ny_os,
					  'Nz_os':Nz_os,
					  'N_slices':N_slices,
					  'theta_list':theta_list_curr,
					  'z_mn_list':z_mn_list_curr,
					  'Nz_sta':Nz_sta,
					  'Nz_end':Nz_end,
					  'Nadd':Nadd,
					  'q_z_mesh':q_z_mesh,
					  'Delta_z':Delta_z}

		M_in = {'Fourier Intensity list':I_data_list_curr,
				'N_p_curr':N_p_curr,
				'InDict_calc_Iandc':InDict_calc_Iandc,
				'N_s':N_slices}

		S_in = {'N_p_curr':N_p_curr,
				'InDict_x2f':InDict_x2f,
				'InDict_f2x':InDict_f2x}

		InDict_IPA = {'N_p_curr':N_p_curr,
					  'N_s':N_slices}



		# Initialise iterate
		if (it_outer == 0):
			if (starting_iterate == 0): # Ground truth for testing
				x = f2x(f_true_os + 0j, N_p=N_p_curr, InDict=InDict_f2x)
			elif(starting_iterate == 1): # Random start
				x = f2x(np.random.rand(Nx_os, Ny_os, Nz_os), N_p=N_p_curr, InDict=InDict_f2x)
			elif(starting_iterate == 2): # Slightly perturbed
				x = f2x(f_true_os + 0.1 * np.random.rand(Nx_os, Ny_os, Nz_os), N_p=N_p_curr, InDict=InDict_f2x)
		else:
			x = f2x(f_avg, N_p=N_p_curr, InDict=InDict_f2x)


		# Inner loop (IPA)
		for it_inner in range(0, it_inner_max):

			# Projection algorithm switching
			if (np.mod(it_inner, it_inner_cycle) < it_inner_IPA):
				if IPA_sel == 'RAAR':
					IPA = 'RAAR'
					x_new, x_PS, x_PM = RAAR(x, beta, InDict_IPA, M_in, S_in)
				elif IPA_sel == 'ER':
					IPA = 'ER'
					x_PM = P_M(x, M_in)
					x_PS = P_S(x_PM, S_in)
					x_new = x_PS
			else:
				IPA = 'ER'
				x_PM = P_M(x, M_in)
				x_PS = P_S(x_PM, S_in)
				x_new = x_PS

			# Update iterate
			x = x_new

			#-------------------------
			# Error calculations
			x_est = x_PS
			f_est = x2f(x_est, N_p=N_p_curr, InDict=InDict_x2f)
			I_est, _ = calc_Iandc(x_est, N_p=N_p_curr, InDict=InDict_calc_Iandc)

			errors_inner[it_inner,0] = np.sum(np.abs(f_est - f_true_os)**2)
			errors_inner[it_inner,1] = calc_error_1DList(I_est, I_data_list_curr)

			# Checking if constraint is satisfied
			I_PM, _ = calc_Iandc(x_PM, N_p=N_p_curr, InDict=InDict_calc_Iandc)
			errors_PM[it_inner] = calc_error_1DList(I_PM, I_data_list_curr)

			print("it_inner=%d, %.3e, %.3e" % (it_inner,errors_inner[it_inner,0], errors_inner[it_inner,1]))

			#-------------------------
			# Keep the best reconstruction so far as measured by the data error
			if (errors_inner[it_inner,1] < E_inner_best):
				x_best = x_est
				E_inner_best = errors_inner[it_inner,1]
			#-------------------------


		# Accumulate
		f_est = x2f(x_best, N_p=N_p_curr, InDict=InDict_x2f)
		f_sum_curr += f_est
		errors_inner_0_sum += errors_inner[:,0]
		errors_inner_1_sum += errors_inner[:,1]

	# Compute averages
	f_avg = f_sum_curr / N_worker
	errors[it_outer*it_inner_max:(it_outer+1)*it_inner_max, 0] = errors_inner_0_sum / N_worker
	errors[it_outer*it_inner_max:(it_outer+1)*it_inner_max, 1] = errors_inner_1_sum / N_worker

	# Print some outputs to monitor progress
	sys.stdout.write('it_outer = %d, e = %.3e, E = %.3e \n' % (it_outer, errors[(it_outer+1)*it_inner_max-1,0], errors[(it_outer+1)*it_inner_max-1,1]))
	sys.stdout.flush()

	sys.stdout.write("Saving checkpoint \n")
	sys.stdout.flush()

	np.savez(save_filename + '.npz', errors=errors
						   		   , f_avg=f_avg
								   , rotation_scenario=rotation_scenario
								   , defocus_scenario=defocus_scenario
								   , starting_iterate=starting_iterate
								   , N_padding=N_padding
								   , lamb=lamb
								   , N_worker=N_worker
								   , it_outer_max=it_outer_max
								   , it_inner_max=it_inner_max
								   , it_inner_cycle=it_inner_cycle
								   , it_inner_IPA=it_inner_IPA
								   , it_inner=it_inner
								   , it_outer=it_outer
								   , N_views=N_views
								   , N_slices=N_slices
								   , theta_list=theta_list
								   , z_mn_list=z_mn_list
								   , Nz_sta=Nz_sta
								   , Nz_end=Nz_end
								   , Nadd=Nadd
								   , f_true=f_true_os[Nx_sta:Nx_end, Ny_sta:Ny_end, Nz_sta:Nz_end]
								   , I_data_list=I_data_list
								   , Nx_os=Nx_os
								   , Ny_os=Ny_os
								   , Nz_os=Nz_os
								   , N=N
								   , run_num=run_num
								   , Delta_z=Delta_z)
	sys.stdout.write("Finished Saving checkpoint \n")
	sys.stdout.flush()

