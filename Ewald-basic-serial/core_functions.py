"""
Functions for reconstructing from curved Ewald spheres.

Date Created: 24 Oct 2020
Last Modified: 18 Aug 2021
Author: JC
"""

import numpy as np
from numpy.fft import fftn, ifftn, fftshift, ifftshift, fft, ifft
from reborn.misc.rotate import Rotate3D

import scipy


def L_fwd(f_os, z_mn, q_z_mesh, Delta_z):
	"""
	Take a 2D slice and propagate it forward by distance z_mn.
	"""
	return ifftn(np.exp(1j * z_mn * q_z_mesh) * fftn(f_os)) * Delta_z


def L_inv(f_os, z_mn, q_z_mesh, Delta_z):
	"""
	Take a 2D slice and propagate it back by distance z_mn.
	"""
	return ifftn(np.exp(1j * -z_mn * q_z_mesh) * fftn(f_os)) / Delta_z


def calc_Iandc(x, N_p, InDict):
	"""
	Input:
	x       - the iterate
	N_p     - number of projections
	InDict - Dictionary holding any inputs needed. 

	Output:
	I_list - Projected intensity list
	c_list - cluster amplitudes (averaged complex amplitudes from all propagated slices)
	"""
	
	Nx_os = InDict['Nx_os']
	Ny_os = InDict['Ny_os']
	N_s = InDict['N_slices']

	I_list = [0 for n_p in range(N_p)]
	c_list = [0 for n_p in range(N_p)]

	for n_p in range(N_p):
		# Allocate memory
		psi = np.zeros([Nx_os, Ny_os], dtype=np.complex)

		# Sum all propagated slices
		for n_s in range(N_s):
			psi += x[n_p][n_s]

		c_list[n_p] = psi / N_s
		I_list[n_p] = np.imag(psi)

	return I_list, c_list


def f2x(f, N_p, InDict):
	"""
	Takes a zero-padded f and generate all oversampled, complex amplitude propagated slices at the correct orientations.
	"""
	
	N_s = InDict['N_slices']
	theta_list = InDict['theta_list']
	z_mn_list = InDict['z_mn_list']
	Nz_sta = InDict['Nz_sta']
	Nz_end = InDict['Nz_end']
	Nadd = InDict['Nadd']
	q_z_mesh = InDict['q_z_mesh']
	Delta_z = InDict['Delta_z']

	x = [[0 for n_s in range(N_s)] for n_p in range(N_p)]
	
	# Define a reborn rotation class with the f passed in as an attribute
	rot_class = Rotate3D(f)

	# Rotate the cube so that all slices are facing the same way, then propagate.
	for n_p in range(N_p):
		#-----------------------
		scipy_rot_obj = scipy.spatial.transform.Rotation.from_euler('zyz', [theta_list[0][n_p], theta_list[1][n_p], theta_list[2][n_p]], degrees=False)
		
		rot_class.rotation(scipy_rot_obj)
		f_rotated = rot_class.f
		#-----------------------
		# Original code
		# f_rotated = Rotate3D(f = f, 
		# 					   euler_angles = np.array([theta_list[0][n_p], theta_list[1][n_p], theta_list[2][n_p]]))
		#-----------------------

		for n_s in range(Nz_sta, Nz_end): # Only go from one end of the object to the other end, don't loop through the whole zero-padded array.
			x[n_p][n_s-Nadd] = L_fwd(f_rotated[n_s,:,:], z_mn_list[n_p, n_s-Nadd], q_z_mesh, Delta_z) # The -Nadd is to bring the index back to zero.

	return x


def x2f(x, N_p, InDict):

	Nx_os = InDict['Nx_os']
	Ny_os = InDict['Ny_os']
	Nz_os = InDict['Nz_os']
	N_s = InDict['N_slices']
	theta_list = InDict['theta_list']
	z_mn_list = InDict['z_mn_list']
	Nz_sta = InDict['Nz_sta']
	Nz_end = InDict['Nz_end']
	Nadd = InDict['Nadd']
	q_z_mesh = InDict['q_z_mesh']
	Delta_z = InDict['Delta_z']

	# Step 1.
	# Inverse propagate
	x_Linv = [[0 for n_s in range(N_s)] for n_p in range(N_p)]
	for n_p in range(N_p):
		for n_s in range(N_s):
			x_Linv[n_p][n_s] = L_inv(x[n_p][n_s], z_mn_list[n_p, n_s], q_z_mesh, Delta_z) # No -Nadd here because will do it later.

	# Step 2.
	# Form 3D volumes out of the slices and inverse rotate then average them
	f_new = np.zeros((Nx_os, Ny_os, Nz_os), dtype=np.complex)
	for n_p in range(N_p):
		# Place slices into the n_p's 3D volume
		f_new_np = np.zeros((Nx_os, Ny_os, Nz_os), dtype=np.complex)
		for n_s in range(Nz_sta, Nz_end):
			f_new_np[n_s,:,:] = x_Linv[n_p][n_s-Nadd]

		#-----------------------
		# Define a reborn rotation class with the f passed in as an attribute
		rot_class = Rotate3D(f_new_np)

		# Inverse rotate
		scipy_rot_obj = scipy.spatial.transform.Rotation.from_euler('zyz', [-theta_list[2][n_p], -theta_list[1][n_p], -theta_list[0][n_p]], degrees=False)

		rot_class.rotation(scipy_rot_obj)
		f_new_np = rot_class.f
		#-----------------------
		# Inverse rotate - original code
		# f_new_np = Rotate3D(f = f_new_np,
		# 					euler_angles = np.array([-theta_list[2][n_p], -theta_list[1][n_p], -theta_list[0][n_p]])) # Note the order of theta_list is reversed
		#-----------------------


		# Accumulate
		f_new += f_new_np
	# Calculate the average
	f_new /= N_p

	return f_new


def calc_error_1DList(x, x_0):
	"""
	Calculate the summed squared difference between all elements in two 1D arrays.
	"""

	error_val = 0
	for m in range(len(x)):
		error_val += np.sum(np.abs(x[m] - x_0[m])**2)

	return error_val
