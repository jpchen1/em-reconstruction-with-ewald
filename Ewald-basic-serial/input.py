"""
Functions to simulate an input object potential.

Date Created: 24 Oct 2020
Last Modified: 6 Jan 2021
Author: JC
"""

import numpy as np
import sys
from reborn.target import crystal
import scipy.constants as const
from reborn.target import molecule
from core_functions import *


# The mean of the object potential
OBJECT_MEAN = 100


def make_particle(pdb_file_path, resolution):
	"""
	Make a simulated virus particle.
	The virus capsid at the center of the unit cell was taken as the particle 
	to be reconstructed. Since the PDB file only provided the atomic coordinates 
	of the capsid, the interior of the virus was filled with the average 
	electron density of the capsid for our simulations. 
	"""
	
	eV = const.value('electron volt')
	photon_energy_ev = 12000 * eV

	# The cryst class has molecule, unit cell, and spacegroup info
	cryst = crystal.CrystalStructure(pdb_file_path, tight_packing=True, expand_ncs_coordinates=True)
	uc = cryst.unitcell
	sg = cryst.spacegroup
	print(uc)
	print(sg)

	# Density map configuration with spacegroup considerations
	cdmap = crystal.CrystalDensityMap(cryst=cryst, resolution=resolution, oversampling=1)

	# Scattering factors
	f = cryst.molecule.get_scattering_factors(photon_energy=photon_energy_ev)
	f = np.round(np.real(f)) # Convert scattering factors into just the atomic numbers
	print('sum over f', np.sum(f))

	au_map = cdmap.place_atoms_in_map(cryst.fractional_coordinates, f, mode='trilinear')
	print('sum over au_map', np.sum(au_map))


	#----------------------------
	# Create the virus density
	# The central virus particle in the unit cell are mapped by spacegroup operations 12-24
	rho = 0
	for k in range(cryst.spacegroup.n_operations):
	    if k >= 12:
	        print(f'Adding asymmetric unit {k}')
	        rho += cdmap.au_to_k(k, au_map)
	print('sum over rho', np.sum(rho))

	#----------------------------
	# Fill in the virus
	from scipy.ndimage.morphology import binary_fill_holes
	from scipy.ndimage import binary_dilation

	mask_shell = (np.abs(rho) > 0) + 0.0

	Nx, Ny, Nz = mask_shell.shape
	Nx_cent = int(np.round(Nx/2))
	Ny_cent = int(np.round(Ny/2))
	Nz_cent = int(np.round(Nz/2))

	mask_filled = np.zeros((Nx,Ny,Nz))

	# Dilate the shell density until it is able to encircle the virus for 
	# the binary_fill_holes function to be able to find the hole.
	it_total_dilation = 0
	while mask_filled[Nx_cent,Ny_cent,Nz_cent] == 0:
		mask_shell = binary_dilation(mask_shell, iterations=1) # Dilate
		mask_filled = binary_fill_holes(mask_shell) + 0.0
		it_total_dilation += 1
		print("Number of dilations done:", it_total_dilation)
	print("Dilation Finished.")
	print("Centre value:", mask_filled[Nx_cent,Ny_cent,Nz_cent])


	mask_interior = mask_filled - mask_shell
	# Dilate the interior to get it right up against (or even maybe within) the shell (otherwise it may leave a gap of zeros around the interior which would be unrealistic).
	mask_interior = binary_dilation(mask_interior, iterations=3)

	# Now set the interior of the virus to have some density and add it to the original virus shell to create a filled virus.
	# Choose to set it to the mean density value of the shell.
	density_value_interior = np.mean(rho[rho > 0])
	print(f"Density_value_interior: {density_value_interior:.3f}")
	rho_filled = rho + mask_interior * density_value_interior

	# Set the mean of the object to be object_mean
	print("Original mean of object:", np.mean(rho_filled[rho_filled != 0]))
	rho_filled *= OBJECT_MEAN / np.mean(rho_filled[rho_filled != 0])
	print("Adjusted mean of object:", np.mean(rho_filled[rho_filled != 0]))

	# Distance between slices - i.e. the voxel size
	# For example: UC 33.8nm, cubic, 73 voxels / 86 voxels total in array = 28.7nm
	Delta_z = uc.a*1e9 / Nz

	# Return the filled density and the distance between slices in nm (assume unit cell to be cubic so only returning 'a').
	return rho_filled, Delta_z


def get_qz(lamb, Delta_z, N, L):
	""" 
	Calculate the q_z meshgrid 
	"""

	# Resolution defined also as the voxel size.
	d = Delta_z

	# Wavenumber
	k_wavenumber = (2*np.pi) / lamb # (nm^-1)

	# First compute the scattering angle.
	theta_scatt = 2 * np.arcsin(lamb / (2*d))

	# Get the maximum q magnitude (defined to go to the sides of the computational array, not to the corners).
	q_mag = 4*np.pi * np.sin(theta_scatt/2) / lamb

	# Calcuate the maximum q's in each direction
	q_x_max = q_mag * np.cos(theta_scatt/2)
	q_y_max = q_mag * np.cos(theta_scatt/2)

	# Form the mesh grid in qx-qy
	q_x_mesh, q_y_mesh = np.meshgrid(np.linspace(-q_x_max, q_x_max, N), 
								     np.linspace(-q_y_max, q_y_max, N))

	# Calculate qz
	q_z_mesh = np.sqrt(k_wavenumber**2 - q_x_mesh**2 - q_y_mesh**2) - k_wavenumber


	# fftshift the qz mesh for later computational speed.
	q_z_mesh = ifftshift(q_z_mesh)


	# Ewald curvature number - (the epsilon in the paper)
	ewald_curvature_number = (2 * d**2) / (lamb * L)


	sys.stdout.write("-"*27 + '\n')
	sys.stdout.write(f"Wavelength = {lamb*1e-9:.3e} m" + '\n')
	sys.stdout.write(f"Delta_z (same as resolution d) = {Delta_z:.3f} nm" + '\n')
	sys.stdout.write(f"Resolution (d) = {d:.3f} nm" + '\n')
	sys.stdout.write(f"Size of object (L) = {L:.3f} nm" + '\n')
	sys.stdout.write("DOF = %.3f nm" % (2*Delta_z**2/lamb) + '\n')
	sys.stdout.write("Resolution when Ewald curvature is significant: %.3f nm" % np.sqrt(lamb*L/2) + '\n')
	sys.stdout.write("Scattering angle (theta): %.3f deg" % (theta_scatt * 180/np.pi) + '\n')
	sys.stdout.write("Ewald number = %.3f" % ewald_curvature_number + '\n')
	sys.stdout.write("-"*27 + '\n')
	sys.stdout.flush()

	return q_z_mesh


def rotation_setup(rotation_scenario, N_views):
	"""
	Rotation setup (all angles in theta_list are in radians)
	"""

	if (rotation_scenario == 0):
		theta_list = [0 for i in range(3)]
		theta_list[0] = np.zeros(N_views)
		theta_list[1] = np.zeros(N_views)
		theta_list[2] = np.zeros(N_views)
	elif (rotation_scenario == 1):
		# Scenario 1 - No rotation
		pass
	elif (rotation_scenario == 2):
		# Scenario 2 - Regularly sampled rotation in z
		theta_list = [0 for i in range(3)]
		theta_list[0] = np.linspace(0,360, N_views, endpoint=False) * (np.pi/180)
		theta_list[1] = np.zeros(N_views)
		theta_list[2] = np.zeros(N_views)
	elif (rotation_scenario == 3):
		# Scenario 3 - Regularly sampled rotation in y
		theta_list = [0 for i in range(3)]
		theta_list[0] = np.zeros(N_views)
		theta_list[1] = np.linspace(0,360, N_views, endpoint=False) * (np.pi/180)
		theta_list[2] = np.zeros(N_views)
	elif (rotation_scenario == 4):
		# Scenario 4 - Regularly sampled rotation in all three directions
		# (TO DO) need meshgrid type functions
		theta_list = [0 for i in range(3)]
		theta_list[0] = np.linspace(0,360, N_views, endpoint=False)  * (np.pi/180)
		theta_list[1] = np.linspace(-90,90, N_views, endpoint=False) * (np.pi/180)
		theta_list[2] = np.linspace(0,360, N_views, endpoint=False)  * (np.pi/180)
	elif (rotation_scenario == 5):
		# Scenario 5 - Random rotation
		theta_list = [[0 for i in range(N_views)] for j in range(3)] # 3 because there are 3 Euler angles
		
		# Use scipy's built-in function to generate Euler angles that forms uniformly random orientations
		from scipy.spatial.transform import Rotation as R
		EAngRandom = R.random(N_views, random_state=1234).as_euler('zyz', degrees=False)

		theta_list[0] = EAngRandom[:,0]
		theta_list[1] = EAngRandom[:,1]
		theta_list[2] = EAngRandom[:,2]

	theta_list = np.array(theta_list)

	return theta_list


def defocus_setup(defocus_scenario, N_views, L, N_slices, Delta_z, is_proper_algo):
	"""
	Defocus setup
	"""

	if (defocus_scenario == 1):
		# Scenario 1
		# All focused on the central plane 
		focalPlane_list = np.zeros(N_views)
		
	elif (defocus_scenario == 2):
		# Scenario 2
		# Random focus ranging from |...|obj|...| (L on either side of the object)
		focalPlane_max = 2*L # 2*L - L/2
		focalPlane_min = -L  #-L/2
		focalPlane_list = np.random.rand(N_views) * (focalPlane_max - focalPlane_min) + focalPlane_min

	elif (defocus_scenario == 3):
		# Scenario 3
		# Through-focus series
		focalPlane_list = np.linspace(-L/2, 2*L-L/2, N_slices)


	if is_proper_algo == True:
		# Define the distance of all slices to the propagation plane
		z_mn_list = np.zeros((N_views, N_slices))
		for n_p in range(N_views):
			d_n = focalPlane_list[n_p]
			for n_s in range(N_slices):
				z_mn_list[n_p, n_s] = d_n - n_s * Delta_z
	else:
		# Assume flat Ewald sphere.
		# Redefine the z list by leaving out the term causing the curved Ewald sections.
		z_mn_list = np.zeros((N_views, N_slices))
		for n_p in range(N_views):
			d_n = focalPlane_list[n_p]
			for n_s in range(N_slices):
				z_mn_list[n_p, n_s] = d_n #- n_s * Delta_z <----- this term is left out.


	return z_mn_list


def get_data(pdb_file_path, resolution, lamb, N_padding, N_views_factor, rotation_scenario, defocus_scenario, is_proper_algo):
	"""
	Generate the data for the reconstruction problem.
	"""

	# Make the object
	f_true, Delta_z = make_particle(pdb_file_path, resolution)

	Nx, Ny, Nz = f_true.shape
	sys.stdout.write(f"Shape of f_true: {Nx}, {Ny} {Nz}" + '\n')
	sys.stdout.flush()

	# Assume for now a cubic array for the object (needs to be even).
	N0 = Nx 

	#------------------------
	# Size of the virus particle
	# Find the binary mask region of the virus and use that to figure out its size.
	x_vec, y_vec, z_vec = np.where(f_true > 0)

	# Size of the virus particle in voxels
	L_voxel_z = np.max(z_vec) - np.min(z_vec) 
	L_voxel_x = np.max(x_vec) - np.min(x_vec)
	L_voxel_y = np.max(y_vec) - np.min(y_vec)
	L = L_voxel_z * Delta_z # Size of the virus particle in length units

	#------------------------
	N_slices = N0 # Number of slices (sections)
	N_views = int(np.round(N_views_factor * N0)) # Number of view data

	sys.stdout.write(f"N_views: {N_views}" + '\n')
	#------------------------
	# Calculate padding needed for rotation in a cubic array and propagation.
	N = int(np.ceil(L_voxel_z * np.sqrt(2)) + N_padding)
	Nadd = int((N - N0)/2)

	sys.stdout.write("-"*27 + '\n')
	sys.stdout.write(f"Nadd: {Nadd}" + '\n')
	sys.stdout.write(f"Size of array (N): {N}" + '\n')
	sys.stdout.flush()

	# Size of the zero-padded array (assume cubic for now)
	Nx_os = N
	Ny_os = N
	Nz_os = N

	# Starting index of the object embedded in the zero-padded array
	Nx_sta = Nadd
	Nx_end = Nadd + N0
	Ny_sta = Nadd
	Ny_end = Nadd + N0
	Nz_sta = Nadd
	Nz_end = Nadd + N0

	# Embed the true density in padded array
	f_true_os = np.zeros((N,N,N),dtype=np.complex128)
	f_true_os[Nx_sta:Nx_end, Ny_sta:Ny_end, Nz_sta:Nz_end] = f_true
	#------------------------

	q_z_mesh = get_qz(lamb, Delta_z, N, L)

	theta_list = rotation_setup(rotation_scenario, N_views)

	z_mn_list = defocus_setup(defocus_scenario, N_views, L, N_slices, Delta_z, is_proper_algo)

	# Ingredients needed for core functions
	InDict_f2x = {'N_slices':N_slices,
				  'theta_list':theta_list,
				  'z_mn_list':z_mn_list,
				  'Nz_sta':Nz_sta,
				  'Nz_end':Nz_end,
				  'Nadd':Nadd,
				  'q_z_mesh': q_z_mesh,
				  'Delta_z': Delta_z}

	InDict_calc_Iandc = {'Nx_os':Nx_os,
						 'Ny_os':Ny_os,
						 'N_slices':N_slices}

	# Construct x_true
	x_true = f2x(f_true_os, N_p=N_views, InDict=InDict_f2x)

	# Construct intensity data set
	I_data_list, _ = calc_Iandc(x_true, N_p=N_views, InDict=InDict_calc_Iandc)


	container = {'N_slices':N_slices,
			   'theta_list':theta_list,
			   'z_mn_list':z_mn_list,
			   'Nz_sta':Nz_sta,
			   'Nz_end':Nz_end,
			   'Nadd':Nadd,
			   'q_z_mesh':q_z_mesh,
			   'N_views':N_views,
			   'Nx_os':Nx_os,
			   'Ny_os':Ny_os,
			   'Nz_os':Nz_os,
			   'N':N,
			   'Delta_z':Delta_z,
			   'Nx_sta':Nx_sta,
			   'Ny_sta':Ny_sta,
			   'Nz_sta':Nz_sta,
			   'Nx_end':Nx_end,
			   'Ny_end':Ny_end,
			   'Nz_end':Nz_end,
			   'N':N}

	return I_data_list, f_true_os, container
