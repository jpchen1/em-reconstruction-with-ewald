"""
Script to try and calculate the Fourier Shell Correlation FSC between two volumes.

Compares the FSC of two volumes.

Loads the npz file from the reconstruction script

Date Created: 24 Oct 2020
Last Modified: 7 Feb 2021
Author: Joe Chen
"""

import numpy as np
import sys
from core_functions import calc_error_1DList

from reborn.utils import make_label_radial_shell, radial_stats, get_FSC

#========================================================================
# Program parameters

# Saving figure?
is_save_fig = 1

# Running on a cluster?
is_remote_running = 0

# Specify resolution
res = 'low'

# File names to plot
file_name1 = "run1_Nworker=3_out=1_in=2.npz"
file_name2 = "run1_flatEwaldAlgo_Nworker=3_out=1_in=2.npz" # Assume flat Ewald

# Number of radial bins for calculating the FSC
N_radials = 25

#========================================================================
# Plotting parameters 
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.axes_grid1 import make_axes_locatable


if (is_remote_running == 1):
	import matplotlib
	matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab!
else:
	plt.close('all')


font_size = 12
fig_width = 15
fig_height = 7
mpl.rcParams['xtick.labelsize'] = font_size
mpl.rcParams['ytick.labelsize'] = font_size
fontsize_graph_labels = 15
CMAP = "gray"

#========================================================================
# Define some helper functions

def extract_data(file_name):
	"""
	Function to extract the data
	"""

	# Load the files
	data = np.load(file_name)

	# Extract the data
	errors = data['errors']
	f_avg=data['f_avg']
	it_outer_max=data['it_outer_max']
	it_inner_max=data['it_inner_max']
	N_views=data['N_views']
	N_slices=data['N_slices']
	theta_list=data['theta_list']
	z_mn_list=data['z_mn_list']
	Nz_sta=data['Nz_sta']
	Nz_end=data['Nz_end']
	Nadd=data['Nadd']
	N=data['N']
	f_true=data['f_true']
	I_data_list=data['I_data_list']
	Delta_z = data['Delta_z']

	return errors, f_avg, I_data_list, f_true, it_outer_max, it_inner_max, N, N_views, N_slices, theta_list, z_mn_list, Nz_sta, Nz_end, Nadd, Delta_z

#========================================================================
# Extract data
errors1, f_est1, I_data_list, f_true, it_outer_max, it_inner_max, N, N_views, N_slices, theta_list, z_mn_list, Nz_sta, Nz_end, Nadd, Delta_z = extract_data(file_name1)
errors2, f_est2, _, _, _, _, _, _, _, _, _, _, _, _, _ = extract_data(file_name2)

#========================================================================
# Calculate size variables

it_total = it_outer_max * it_inner_max

# Make f_true real
f_true = np.abs(f_true)

Nx, Ny, Nz = f_true.shape
N0 = Nx
Nadd = int((N - N0)/2)

Nx_os = N
Ny_os = N
Nz_os = N

Nx_os_cent = int(np.floor(Nx_os/2))
Ny_os_cent = int(np.floor(Ny_os/2))
Nz_os_cent = int(np.floor(Nz_os/2))

Nx_sta = Nadd
Nx_end = Nadd + N0
Ny_sta = Nadd
Ny_end = Nadd + N0
Nz_sta = Nadd
Nz_end = Nadd + N0

# Embed the true density in padded array
f_true_os = np.zeros((N,N,N),dtype=np.float64)
f_true_os[Nx_sta:Nx_end, Ny_sta:Ny_end, Nz_sta:Nz_end] = f_true


Nx_os, Ny_os, Nz_os = f_true_os.shape
Nx_os_cent = int(np.round(Nx_os/2))
Ny_os_cent = int(np.round(Ny_os/2))
Nz_os_cent = int(np.round(Nz_os/2))

print(Nx_os, Ny_os, Nz_os)

#========================================================================
# Normalise errors
norming_const_x = np.sum((f_true)**2)

norming_const_I = calc_error_1DList([np.zeros((Nx_os,Ny_os)) for n_p in range(N_views)], I_data_list)
errors1[:,0] = np.sqrt(errors1[:,0]/norming_const_x)
errors1[:,1] = np.sqrt(errors1[:,1]/norming_const_I)

norming_const_I = calc_error_1DList([np.zeros((Nx_os,Ny_os)) for n_p in range(N_views)], I_data_list)
errors2[:,0] = np.sqrt(errors2[:,0]/norming_const_x)
errors2[:,1] = np.sqrt(errors2[:,1]/norming_const_I)

#========================================================================
# Figures for the paper

#######################
# Figure 1 - Results
#######################

sys.stdout.write("Plotting figure - recons \n")
sys.stdout.flush()

fig_width = 7.5
fig_height = 9.0

fig = plt.figure(figsize=(fig_width, fig_height))
fig_numRow = 4
fig_numCol = 3

# Ground truth
clim_min = np.min(f_true_os) # Make the colour scales range from the same values as the ground truth - for both the recon and the ground truth
clim_max = np.max(f_true_os)

ax = plt.subplot2grid((fig_numRow, fig_numCol), (0, 0))
im = ax.imshow((f_true_os[:,:,Nz_os_cent]), clim=[clim_min, clim_max], interpolation='nearest', cmap=CMAP, origin='lower')
ax.axis('off')
ax = plt.subplot2grid((fig_numRow, fig_numCol), (0, 1))
im = ax.imshow((f_true_os[:,Ny_os_cent,:]), clim=[clim_min, clim_max], interpolation='nearest', cmap=CMAP, origin='lower')
ax.axis('off')
ax = plt.subplot2grid((fig_numRow, fig_numCol), (0, 2))
im = ax.imshow((f_true_os[Nx_os_cent,:,:]), clim=[clim_min, clim_max], interpolation='nearest', cmap=CMAP, origin='lower')
ax.axis('off')

# Reconstruction - 1

# Scaled to have the same mean as the ground truth
f_est = np.abs(f_est1) 
f_est *= np.mean(f_true_os)/np.mean(f_est)

ax = plt.subplot2grid((fig_numRow, fig_numCol), (1, 0))
im = ax.imshow((f_est[:,:,Nz_os_cent]), clim=[clim_min, clim_max], interpolation='nearest', cmap=CMAP, origin='lower')
ax.axis('off')
ax = plt.subplot2grid((fig_numRow, fig_numCol), (1, 1))
im = ax.imshow((f_est[:,Ny_os_cent,:]), clim=[clim_min, clim_max], interpolation='nearest', cmap=CMAP, origin='lower')
ax.axis('off')
ax = plt.subplot2grid((fig_numRow, fig_numCol), (1, 2))
im = ax.imshow((f_est[Nx_os_cent,:,:]), clim=[clim_min, clim_max], interpolation='nearest', cmap=CMAP, origin='lower')
ax.axis('off')


# Reconstruction - 2
# Scaled to have the same mean as the ground truth
f_est = np.abs(f_est2) 
f_est *= np.mean(f_true_os)/np.mean(f_est)

ax = plt.subplot2grid((fig_numRow, fig_numCol), (2, 0))
im = ax.imshow((f_est[:,:,Nz_os_cent]), clim=[clim_min, clim_max], interpolation='nearest', cmap=CMAP, origin='lower')
ax.axis('off')
ax = plt.subplot2grid((fig_numRow, fig_numCol), (2, 1))
im = ax.imshow((f_est[:,Ny_os_cent,:]), clim=[clim_min, clim_max], interpolation='nearest', cmap=CMAP, origin='lower')
ax.axis('off')
ax = plt.subplot2grid((fig_numRow, fig_numCol), (2, 2))
im = ax.imshow((f_est[Nx_os_cent,:,:]), clim=[clim_min, clim_max], interpolation='nearest', cmap=CMAP, origin='lower')
ax.axis('off')

# Error plot
ax = plt.subplot2grid((fig_numRow, fig_numCol), (3, 0), colspan=3)
ax.plot(np.arange(0,it_total), (errors1[:,0]), lw=2, color='tab:blue', linestyle='-')
ax.plot(np.arange(0,it_total), (errors1[:,1]), lw=2, color='tab:blue', linestyle='--')
ax.plot(np.arange(0,it_total), (errors2[:,0]), lw=2, color='tab:orange', linestyle='-')
ax.plot(np.arange(0,it_total), (errors2[:,1]), lw=2, color='tab:orange', linestyle='--')
ax.set_xlim(0, it_total)
ax.grid()
ax.set_xlabel('Iteration', fontsize=12)
ax.set_ylabel(r'log$_{10}$ Error', fontsize=12)

# Common colourbar to one side
fig.subplots_adjust(right=0.8)
cbar_ax = fig.add_axes([0.83, 0.31, 0.02, 0.57])
fig.colorbar(im, cax=cbar_ax)


if (is_save_fig == 1):
	plt.savefig(f"results_recon" + ".png", dpi=200)
else:
	plt.show()



########################
# Figure 2 - Data views
########################
sys.stdout.write("Plotting figure - data \n")
sys.stdout.flush()

fig_width = 3.5
fig_height = 7
fig = plt.figure(figsize=(fig_width, fig_height))
fig_numRow = 3
fig_numCol = 1


# Set the Clim and max to the same value throughout the 6 subfigures
clim_min = np.min(I_data_list[0])
clim_max = np.max(I_data_list[0])

ax = plt.subplot2grid((fig_numRow, fig_numCol), (0, 0))
im = ax.imshow(I_data_list[0], clim=[clim_min, clim_max], interpolation='nearest', cmap=CMAP, origin='lower')
ax.axis('off')
ax = plt.subplot2grid((fig_numRow, fig_numCol), (1, 0))
im = ax.imshow(I_data_list[1], clim=[clim_min, clim_max], interpolation='nearest', cmap=CMAP, origin='lower')
ax.axis('off')
ax = plt.subplot2grid((fig_numRow, fig_numCol), (2, 0))
im = ax.imshow(I_data_list[2], clim=[clim_min, clim_max], interpolation='nearest', cmap=CMAP, origin='lower')
ax.axis('off')

plt.tight_layout()

if (is_save_fig == 1):
	plt.savefig(f"results_data" + ".png", dpi=200)
else:
	plt.show()


#========================================================================
# Calculate the FSC

sys.stdout.write("Making labels\n")
sys.stdout.flush()

q_max = np.pi / Delta_z  # This is when delta x delta q = 2pi/N The other definition gives: 1 / (2*Delta_z) #nm^-1

# Specify the radial bins and make the labels
r_bin_vec = np.linspace(0, Nx_os_cent, N_radials)
labels_radial = make_label_radial_shell(r_bin_vec, N_vec=np.array([Nx_os, Ny_os, Nz_os]))


# Scale the r_bins to get q_bins
q_bin_vec = r_bin_vec / np.max(r_bin_vec) * q_max


# 1/2 bit threshold
num_voxles_per_shell = radial_stats(f=f_est1, labels_radial=labels_radial, N_radials=N_radials, mode="count")
half_bit_thresh = (0.2071 + 1.9102/np.sqrt(num_voxles_per_shell)) / (1.2071 + 0.9102/np.sqrt(num_voxles_per_shell))


########################
# Figure 3 - FSC
########################

sys.stdout.write("Plotting figure - FSC\n")
sys.stdout.flush()

# Calculate the FSC
radial_FSC1 = get_FSC(f_est1, f_true_os, labels_radial, N_radials)
radial_FSC2 = get_FSC(f_est2, f_true_os, labels_radial, N_radials)


fig = plt.figure(figsize=(8, 6))
ax1 = fig.add_subplot(111)
ax2 = ax1.twiny()

ax1.plot(q_bin_vec[:], radial_FSC1, 's-', markerfacecolor='none', markersize=7, lw=2)
ax1.plot(q_bin_vec[:], radial_FSC2, 'o-', markerfacecolor='none', markersize=7, lw=2)
ax1.plot(q_bin_vec[:], half_bit_thresh, 'k--', lw=2)
ax1.grid()
ax1.set_xlabel(r'$q$ (nm$^{-1}$)', fontsize=fontsize_graph_labels)
ax1.set_ylabel('FSC', fontsize=fontsize_graph_labels)
ax1.set_ylim([0,1])
ax1.set_xlim(left=0)

# Making the second x-axis for resolution
if res == 'low':
	x2_tick_locations = np.array([0.5, 1.0, 1.5, 2.0, 2.5, 3.0])
elif res == 'high':
	x2_tick_locations = np.array([2.5, 5.0, 7.5, 10.0, 12.5, 15.0])
	ax1.set_xlim(right=15)
elif res == 'med':
	x2_tick_locations = np.array([1.0, 2.0, 3.0, 4.0, 5.0, 6.0])
	ax1.set_xlim(right=6.5)

def tick_x2_label_gen(X):
    Y = 2*np.pi / X
    if res == 'low':
    	return ["%.1f" % z for z in Y]
    elif res == 'high':
    	return ["%.2f" % z for z in Y]
    elif res == 'med':
    	return ["%.2f" % z for z in Y]

ax2.set_xlim(ax1.get_xlim())
ax2.set_xticks(x2_tick_locations)
ax2.set_xticklabels(tick_x2_label_gen(x2_tick_locations))
ax2.set_xlabel(r"$x$ (nm)", fontsize=fontsize_graph_labels)


if (is_save_fig == 1):
	plt.savefig(f"results_fsc" + ".png", dpi=200)
else:
	plt.show()


